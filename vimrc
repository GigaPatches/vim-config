set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle
call vundle#begin()

" Vundle Plugins
Plugin 'gmarik/vundle'
Plugin 'Valloric/YouCompleteMe'
Plugin 'scrooloose/syntastic'
Plugin 'altercation/vim-colors-solarized'

call vundle#end()

filetype plugin indent on

" begin normal config
syntax on

set backspace=indent,eol,start
set autoindent

set number
set relativenumber

set shiftwidth=4
set softtabstop=4
set expandtab

set cul " cursor line
set cuc " cursor column

set background=dark
set t_Co=256
colorscheme solarized
